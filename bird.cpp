///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file bird.cpp
/// @version 1.0
///
/// Exports data about all birds
///
/// @author Eliya Nakamura <eliyanak@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   18_Feb_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "bird.hpp"

using namespace std;

namespace animalfarm {

void Bird::printInfo() {

   if( isMigratory == true){
      migratory = "true";
   } else if (isMigratory == false) {
      migratory = "false";
   } else {
      migratory = "Unknown";
   }

   Animal::printInfo();
   cout << "   Feather Color = [" << colorName( featherColor ) << "]" << endl;
   cout << "   Is Migratory = [" << migratory << "]" << endl;
}

const string Bird::speak() {
   return string( "Tweet" );
}


} // namespace animalfarm

