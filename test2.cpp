///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file test2.cpp
/// @version 1.0
///
/// Tests linked lists.
///
/// @author Eliya Nakamura <eliyanak@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   25_Mar_21
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include "node.hpp"
#include "list.hpp"

using namespace std;
using namespace animalfarm;

int main() {

   Node node;
   SingleLinkedList list;
   Node* node1 = new Node;
   Node* node2 = new Node;

   // test empty function
   cout << "List is empty? " << boolalpha << list.empty() << endl;

   // test push_front function
   list.push_front(node1);
   list.push_front(node2);

   // print addresses of nodes
   cout << "Pushed two nodes to the front of the list." << endl;
   cout << "Node 1: " << node1 << " | Node 2: " << node2 << endl;

   // test size function
   cout << "There are currently " << list.size() << " nodes in the list." << endl;
   
   // test get_first function
   cout << "The first node in the list is " << list.get_first() << endl;

   // test get_next function 
   cout << "The next node in the list is " << list.get_next( list.get_first() ) << endl;

   // test pop_front function
   list.pop_front();
   cout << "The first item in the list has been popped out." << endl;
   cout << "There are currently " << list.size() << " nodes in the list." << endl;

}
