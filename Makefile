###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 07a - Animal Farm 4
#
# @file    Makefile
# @version 1.0
#
# @author Eliya Nakamura <eliyanak@hawaii.edu>
# @brief  Lab 07a - Animal Farm 4 - EE 205 - Spr 2021
# @date   25_Mar_2021
###############################################################################

all: main

main.o:  animal.hpp factory.hpp node.hpp list.hpp main.cpp
	g++ -c main.cpp

test.o:	animal.hpp factory.hpp test.cpp
	g++ -c test.cpp

test2.o: node.hpp list.hpp test2.cpp
	g++ -c test2.cpp

animal.o: animal.hpp node.hpp animal.cpp
	g++ -c animal.cpp

factory.o: animal.hpp cat.hpp dog.hpp nunu.hpp aku.hpp palila.hpp nene.hpp factory.hpp factory.cpp
	g++ -c factory.cpp

node.o: node.hpp node.cpp
	g++ -c node.cpp

list.o: list.hpp list.cpp
	g++ -c list.cpp

mammal.o: mammal.hpp mammal.cpp
	g++ -c mammal.cpp

fish.o: fish.hpp fish.cpp
	g++ -c fish.cpp

bird.o: bird.hpp bird.cpp
	g++ -c bird.cpp

cat.o: cat.cpp cat.hpp
	g++ -c cat.cpp

dog.o: dog.cpp dog.hpp
	g++ -c dog.cpp

nunu.o: nunu.cpp nunu.hpp
	g++ -c nunu.cpp

aku.o: aku.cpp aku.hpp
	g++ -c aku.cpp

palila.o: palila.cpp palila.hpp
	g++ -c palila.cpp

nene.o: nene.cpp nene.hpp
	g++ -c nene.cpp

main: main.cpp *.hpp main.o animal.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o factory.o node.o list.o
	g++ -o main main.o animal.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o factory.o node.o list.o

test: test.cpp *.hpp test.o animal.o factory.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o
	g++ -o test test.o animal.o factory.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o

test2: test2.cpp *.hpp test2.o node.o list.o
	g++ -o test2 test2.o node.o list.o

clean:
	rm -f *.o main test test2
