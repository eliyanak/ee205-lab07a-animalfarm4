///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file test.cpp
/// @version 1.0
///
/// Tests random attibute generators.
///
/// @author Eliya Nakamura <eliyanak@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   4 March 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "factory.hpp"

using namespace std;
using namespace animalfarm;

int main() {

   // tests random name, color, gender
   Cat testCat(Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender());
   testCat.printInfo();

   // tests random bool, color, gender
   Nunu testNunu(Animal::getRandomBool(), Animal::getRandomColor(), Animal::getRandomGender());
   testNunu.printInfo();

   // tests random weight, color, gender
   Aku testAku(Animal::getRandomWeight(4, 30), Animal::getRandomColor(), Animal::getRandomGender());
   testAku.printInfo();

   // tests class factory
   for ( int i = 0 ; i < 25 ; i++ ) {
      Animal *a = AnimalFactory::getRandomAnimal();
      cout << a->speak() << endl;
   }

}
