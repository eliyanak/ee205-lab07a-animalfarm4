///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.cpp
/// @version 1.0
///
/// Exports data for lists
///
/// @author Eliya Nakamura <eliyanak@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   25_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include "list.hpp"

using namespace std;
using namespace animalfarm;

const bool SingleLinkedList::empty() const {
   // if head points to null, the list is empty
   if (head == nullptr)
      return true;
   else
      return false;
}

void SingleLinkedList::push_front( Node* newNode ) {
   if( newNode == nullptr )
      return;
   newNode -> next = head;
   head = newNode;
}

Node* SingleLinkedList::pop_front() {
   // if the list is already empty, return null
   if (head == nullptr) {
      return nullptr;
   }
      
   Node* tmp = head;
   head = head->next;
   return tmp;
}

Node* SingleLinkedList::get_first() const {
   if (head == nullptr) {
      return nullptr;
   } else {
      return head;
   }
}

Node* SingleLinkedList::get_next( const Node* currentNode ) const {
   if( currentNode -> next == nullptr ) {
      return nullptr;
   } else {
      return currentNode -> next;
   }
}

unsigned int SingleLinkedList::size() const {
   unsigned int count = 0;
   Node* currentNode = head;
   // iterate through each node until it reaches null, indicating the end of the list
   while ( currentNode != nullptr ) {
      currentNode = currentNode -> next;
      count++;
   }
   return count;
}


