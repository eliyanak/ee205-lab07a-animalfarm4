///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file factory.hpp
/// @version 1.0
///
/// Exports data for animal factory
///
/// @author Eliya Nakamura <eliyanak@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   09_Mar_2021
///////////////////////////////////////////////////////////////////////////////


#pragma once

using namespace std;

namespace animalfarm {

class AnimalFactory {
   public:
      static Animal* getRandomAnimal();
};
} // namespace animalfarm
