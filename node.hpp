///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file node.hpp
/// @version 1.0
///
/// Exports data for node
///
/// @author Eliya Nakamura <eliyanak@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   25_Mar_2021
///////////////////////////////////////////////////////////////////////////////


#pragma once

using namespace std;

namespace animalfarm {

class Node {
   protected:
      Node* next = nullptr;
      friend class SingleLinkedList;
};
} // namespace animalfarm

