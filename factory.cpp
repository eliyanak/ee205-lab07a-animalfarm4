///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file factory.cpp
/// @version 1.0
///
/// Exports data for animal factory
///
/// @author Eliya Nakamura <eliyanak@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   09_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <cstdlib>
#include <random>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "factory.hpp"

using namespace std;

namespace animalfarm {

static random_device randomDevice;
static mt19937_64 RNG( randomDevice() );
static bernoulli_distribution boolRNG( 0.5 );

Animal* AnimalFactory::getRandomAnimal() {
   Animal* newAnimal = NULL;
   uniform_int_distribution<> animalRNG(0, 5);

   // generate number from 0 to 5
   int i = animalRNG( RNG );

   switch (i) {
      case 0:  newAnimal = new Cat     (Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender()); break;
      case 1:  newAnimal = new Dog     (Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender()); break;
      case 2:  newAnimal = new Nunu    (Animal::getRandomBool(), RED, Animal::getRandomGender()); break;
      case 3:  newAnimal = new Aku     (Animal::getRandomWeight(4, 30), SILVER, Animal::getRandomGender()); break;
      case 4:  newAnimal = new Palila  (Animal::getRandomName(), YELLOW, Animal::getRandomGender()); break;
      case 5:  newAnimal = new Nene    (Animal::getRandomName(), BROWN, Animal::getRandomGender()); break;
   }

   return newAnimal;
}
} // namespace animalfarm
